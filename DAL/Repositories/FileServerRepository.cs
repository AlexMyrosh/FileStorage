﻿using DAL.Repositories.Interfaces;
using System.IO;
using System.Web;

namespace DAL.Repositories
{
    public class FileRepositoryServer : IFileServerRepository
    {
        public void Add(HttpPostedFileBase upload, string UserName, string FileName)
        {
            string root = Path.GetDirectoryName(Path.GetDirectoryName(HttpContext.Current.Server.MapPath("~")));
            string pathToUserFiles = root + "\\DAL\\Files\\" + UserName;
            Directory.CreateDirectory(pathToUserFiles);
            string path = $"{pathToUserFiles}\\{FileName}";

            upload.SaveAs(path);
        }
        public void Delete(string path)
        {
            File.Delete(path);
        }
        public void DeleteFolderWithFiles(string username)
        {
            string root = Path.GetDirectoryName(Path.GetDirectoryName(HttpContext.Current.Server.MapPath("~")));
            string pathToUserFiles = root + "\\DAL\\Files\\" + username;
            if (Directory.Exists(pathToUserFiles))
            {
                Directory.Delete(pathToUserFiles, true);
            }
        }
        public void Update(string oldUsername, string newUsername)
        {
            string root = Path.GetDirectoryName(Path.GetDirectoryName(HttpContext.Current.Server.MapPath("~")));
            string pathToUserFiles = root + "\\DAL\\Files\\";

            if (Directory.Exists(pathToUserFiles + oldUsername))
            {
                Directory.Move(pathToUserFiles + oldUsername, pathToUserFiles + newUsername);
            }

        }
    }
}