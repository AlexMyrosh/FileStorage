﻿using DAL.Context;
using DAL.Exceptions;
using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class FileDbRepository :IFileDbRepository
    {
        private readonly IFileStorageContext _context;
        public FileDbRepository(IFileStorageContext context)
        {
            _context = context;
        }

        public async Task<FileData> AddAsync(FileData file)
        {
            _context.Set<FileData>().Add(file);
            await _context.SaveChangesAsync();
            return file;
        }

        public async Task<List<FileData>> GetAllAsync()
        {
            return await _context.Set<FileData>().ToListAsync();
        }

        public async Task DeleteAsync(Guid Id)
        {
            FileData file = await _context.Set<FileData>().FindAsync(Id);
            _context.Set<FileData>().Remove(file);
            await _context.SaveChangesAsync();
        }

        public async Task<FileData> UpdateAsync(FileData newFile)
        {
            FileData oldFile = await _context.Set<FileData>().FindAsync(newFile.Id);
            if (oldFile == null) throw new FileStorageException("File wasn`t found to update");


            oldFile.UserFileName = oldFile.UserFileName != null ? newFile.UserFileName : oldFile.UserFileName;
            oldFile.Description = oldFile.Description != null ? newFile.Description : oldFile.Description;
            oldFile.Path = oldFile.Path != null ? newFile.Path : oldFile.Path;

            await _context.SaveChangesAsync();


            return oldFile;
        }

        public async Task<FileData> GetByIdAsync(Guid Id)
        {
            return await _context.Set<FileData>().FindAsync(Id);
        }
    }
}