﻿using DAL.Context;
using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly IFileStorageContext _context;
        public ReviewRepository(IFileStorageContext context)
        {
            _context = context;
        }
        public async Task<Review> AddAsync(Review review)
        {
            _context.Set<Review>().Add(review);
            await _context.SaveChangesAsync();
            return review;
        }

        public async Task<List<Review>> GetAllAsync()
        {
            return await _context.Set<Review>().ToListAsync();
        }

        public async Task DeleteAsync(Guid Id)
        {
            Review review = await _context.Set<Review>().FindAsync(Id);
            _context.Set<Review>().Remove(review);
            await _context.SaveChangesAsync();
        }
        public async Task MarkReviewsAsDeleted(ICollection<Review> reviews)
        {
            StringBuilder bld = new StringBuilder();
            foreach (var review in reviews)
            {
                bld.Append(review.Username);
                bld.Append(" (deleted)");
                review.Username = bld.ToString();
                bld.Clear();
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateUsername(ICollection<Review> reviews, string newUsername)
        {
            foreach(var review in reviews)
            {
                review.Username = newUsername;
            }
            await _context.SaveChangesAsync();
        }
    }
}