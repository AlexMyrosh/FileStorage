﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace DAL.Repositories.Interfaces
{
    public interface IFileDbRepository
    {
        Task<FileData> AddAsync(FileData file);
        Task<List<FileData>> GetAllAsync();
        Task DeleteAsync(Guid Id);
        Task<FileData> UpdateAsync(FileData newFile);
        Task<FileData> GetByIdAsync(Guid Id);
    }
}
