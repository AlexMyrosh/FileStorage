﻿using System.Web;

namespace DAL.Repositories.Interfaces
{
    public interface IFileServerRepository
    {
        void Add(HttpPostedFileBase upload, string UserName, string FileName);
        void Delete(string path);
        void DeleteFolderWithFiles(string username);
        void Update(string oldUsername, string newUsername);
    }
}
