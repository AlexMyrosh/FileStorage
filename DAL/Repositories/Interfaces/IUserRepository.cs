﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<User> AddAsync(User user);
        Task<List<User>> GetAllAsync();
        Task DeleteAsync(Guid Id);
        Task<User> UpdateAsync(User newUser);
        Task<User> GetByIdAsync(Guid Id);
        Task<User> GetByUsernameAsync(string username);
    }
}
