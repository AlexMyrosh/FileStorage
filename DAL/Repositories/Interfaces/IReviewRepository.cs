﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories.Interfaces
{
    public interface IReviewRepository
    {
        Task<Review> AddAsync(Review review);
        Task<List<Review>> GetAllAsync();
        Task DeleteAsync(Guid Id);
        Task MarkReviewsAsDeleted(ICollection<Review> reviews);
        Task UpdateUsername(ICollection<Review> reviews, string newUsername);
    }
}
