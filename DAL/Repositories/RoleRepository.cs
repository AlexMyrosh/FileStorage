﻿using DAL.Context;
using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class RoleRepository:IRoleRepository
    {
        private readonly IFileStorageContext _context;
        public RoleRepository(IFileStorageContext context)
        {
            _context = context;
        }
        public async Task<List<Role>> GetAllAsync()
        {
            return await _context.Set<Role>().ToListAsync();
        }

        public async Task<Role> GetByIdAsync(Guid Id)
        {
            return await _context.Set<Role>().FindAsync(Id);
        }
    }
}
