﻿using DAL.Context;
using DAL.Exceptions;
using DAL.Models;
using DAL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IFileStorageContext _context;

        public UserRepository(IFileStorageContext context)
        {
            _context = context;
        }

        public async Task<User> AddAsync(User user)
        {
            _context.Set<User>().Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<List<User>> GetAllAsync()
        {
            return await _context.Set<User>().ToListAsync();
        }

        public async Task DeleteAsync(Guid Id)
        {
            var user = await _context.Set<User>().FindAsync(Id);
            _context.Set<User>().Remove(user);
            await _context.SaveChangesAsync();
        }

        public async Task<User> UpdateAsync(User newUser)
        {
            User user = await _context.Set<User>().FindAsync(newUser.Id);
            if (user == null) throw new FileStorageException("User to update was not founded");

            user.Username = newUser.Username ?? user.Username;
            user.Email = newUser.Email ?? user.Email;
            user.Password = newUser.Password ?? user.Password;
            user.Phone = newUser.Phone ?? user.Phone;
            user.BIO = newUser.BIO ?? user.BIO;
            user.DateOfBirth = newUser.DateOfBirth ?? user.DateOfBirth;
            user.Gender = newUser.Gender ?? user.Gender;

            await _context.SaveChangesAsync();

            return user;
        }

        public async Task<User> GetByIdAsync(Guid Id)
        {
            return await _context.Set<User>().FindAsync(Id);
        }

        public async Task<User> GetByUsernameAsync(string username)
        {
            return await _context.Set<User>().FirstOrDefaultAsync(u => u.Username == username);
        }
    }
}
