﻿using DAL.Models;
using System.Data.Entity;

namespace DAL.Context
{
    public class FileStorageContext : DbContext, IFileStorageContext {

        public virtual DbSet<FileData> Files { get; set; }
        public virtual DbSet<Review> Reviews { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
    }
}