﻿using System.Data.Entity;
using System.Threading.Tasks;

namespace DAL.Context
{
    public interface IFileStorageContext
    {
        Task<int> SaveChangesAsync();
        DbSet<T> Set<T>() where T : class;
    }
}
