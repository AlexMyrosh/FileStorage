﻿using DAL.Repositories.Interfaces;

namespace DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IFileDbRepository FileDbRepository { get; }
        IFileServerRepository FileServerRepository { get; }
        IReviewRepository ReviewRepository { get; }
        IRoleRepository RoleRepository { get; }
        IUserRepository UserRepository { get; }
    }
}
