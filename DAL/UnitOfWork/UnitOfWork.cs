﻿using DAL.Context;
using DAL.Repositories;
using DAL.Repositories.Interfaces;

namespace DAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageContext _context;

        private IFileDbRepository _fileDbRepository;
        private IFileServerRepository _fileServerRepository;
        private IReviewRepository _reviewRepository;
        private IRoleRepository _roleRepository;
        private IUserRepository _userRepository;

        public UnitOfWork()
        {
            _context = new FileStorageContext();
        }

        public IFileDbRepository FileDbRepository
        {
            get
            {
                if (_fileDbRepository == null)
                {
                    _fileDbRepository = new FileDbRepository(_context);
                }
                return _fileDbRepository;
            }
        }
        public IReviewRepository ReviewRepository
        {
            get
            {
                if (_reviewRepository == null)
                {
                    _reviewRepository = new ReviewRepository(_context);
                }
                return _reviewRepository;
            }
        }
        public IRoleRepository RoleRepository
        {
            get
            {
                if (_roleRepository == null)
                {
                    _roleRepository = new RoleRepository(_context);
                }
                return _roleRepository;
            }
        }
        public IUserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_context);
                }
                return _userRepository;
            }
        }
        public IFileServerRepository FileServerRepository
        {
            get
            {
                if (_fileServerRepository == null)
                {
                    _fileServerRepository = new FileRepositoryServer();
                }
                return _fileServerRepository;
            }
        }
    }
}
