﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    [Table("Files")]
    public class FileData
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserFileName { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime DateOfUploading { get; set; }
        [Required]
        public string Path { get; set; }
        [Required]
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string IsPrivate { get; set; }
    }
}