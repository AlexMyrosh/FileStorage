﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Models
{
    public class Review
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public DateTime DateTime { get; set; }
        [Required]
        public string Text { get; set; }
        public string Role { get; set; }
        public Guid? UserId { get; set; }
    }
}