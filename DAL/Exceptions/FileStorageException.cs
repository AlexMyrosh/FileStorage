﻿using System;
using System.Runtime.Serialization;

namespace DAL.Exceptions
{
    [Serializable]
    public class FileStorageException : Exception
    {
        public FileStorageException(string message) : base(message)
        {

        }
        protected FileStorageException(SerializationInfo info, StreamingContext context): base(info, context)
        {

        }
    }
}
