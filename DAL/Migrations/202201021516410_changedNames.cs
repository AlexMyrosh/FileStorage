﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedNames : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Files", newName: "FileDatas");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.FileDatas", newName: "Files");
        }
    }
}
