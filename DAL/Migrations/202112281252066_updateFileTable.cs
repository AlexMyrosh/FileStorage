﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateFileTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "Path", c => c.String(nullable: false));
            DropColumn("dbo.Files", "FileData");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Files", "FileData", c => c.Binary(nullable: false));
            DropColumn("dbo.Files", "Path");
        }
    }
}
