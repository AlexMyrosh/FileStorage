﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileTableRename : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.FileDatas", newName: "Files");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Files", newName: "FileDatas");
        }
    }
}
