﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nullableRoleInReview : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "Role", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "Role", c => c.String(nullable: false));
        }
    }
}
