﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateNames : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Username", c => c.String(nullable: false));
            AddColumn("dbo.Reviews", "Username", c => c.String(nullable: false));
            DropColumn("dbo.Users", "Name");
            DropColumn("dbo.Reviews", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reviews", "Name", c => c.String(nullable: false));
            AddColumn("dbo.Users", "Name", c => c.String(nullable: false));
            DropColumn("dbo.Reviews", "Username");
            DropColumn("dbo.Users", "Username");
        }
    }
}
