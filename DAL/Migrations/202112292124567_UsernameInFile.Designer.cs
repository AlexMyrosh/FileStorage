﻿// <auto-generated />
namespace DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class UsernameInFile : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UsernameInFile));
        
        string IMigrationMetadata.Id
        {
            get { return "202112292124567_UsernameInFile"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
