﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isPrivateFile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "isPrivate", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "isPrivate");
        }
    }
}
