﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeBoolToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Files", "isPrivate", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Files", "isPrivate", c => c.Boolean(nullable: false));
        }
    }
}
