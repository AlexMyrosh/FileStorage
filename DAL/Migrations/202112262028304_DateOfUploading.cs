﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DateOfUploading : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Files", "DateOfUploading", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Files", "DateOfUploading");
        }
    }
}
