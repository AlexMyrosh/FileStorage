﻿using DAL.Context;
using DAL.Models;
using DAL.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.DAL
{
    public class RoleRepositoryTests
    {
        private readonly Mock<IFileStorageContext> mock = new();
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task RoleRepository_GetByIdAsync_ReturnsRole(int numberOfRole)
        {
            //Arrange
            Guid testId = GetTestRoles()[numberOfRole].Id;
            Role expected = GetTestRoles()[numberOfRole];
            mock.Setup(repo => repo.Set<Role>().FindAsync(testId).Result).Returns(expected);
            RoleRepository repository = new(mock.Object);
            //Act
            Role actual = await repository.GetByIdAsync(testId);
            //Assert
            Assert.Equal(expected, actual);
        }

        private static List<Role> GetTestRoles()
        {
            var users = new List<Role>
            {
                new Role { Id = new Guid("8d52de6c-4b64-41d9-b9e9-1edb89f47def"), Name = "user"},
                new Role { Id = new Guid("e61cbb15-18df-456b-a3b8-ca914058c6c8"), Name = "admin"}
            };
            return users;
        }
    }
}
