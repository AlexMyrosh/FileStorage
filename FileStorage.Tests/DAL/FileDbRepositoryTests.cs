﻿using DAL.Context;
using DAL.Models;
using DAL.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.DAL
{
    public class FileDbRepositoryTests
    {
        private readonly Mock<IFileStorageContext> mock = new();

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task FileDbRepository_AddAsyncFileData_ReturnsAddedFile(int numberOfFile)
        {
            //Arrange
            FileData expected = GetTestFiles()[numberOfFile];
            mock.Setup(repo => repo.Set<FileData>().Add(expected)).Returns(expected);
            FileDbRepository repository = new(mock.Object);
            //Act
            FileData actual = await repository.AddAsync(expected);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task FileDbRepository_DeleteAsyncFileData_MethodDeleteAsyncInvoke(int numberOfFile)
        {
            //Arrange
            FileData fileToDelete = GetTestFiles()[numberOfFile];
            mock.Setup(repo => repo.Set<FileData>().Remove(fileToDelete)).Returns(fileToDelete);
            mock.Setup(repo => repo.Set<FileData>().FindAsync(fileToDelete.Id).Result).Returns(fileToDelete);
            FileDbRepository repository = new(mock.Object);
            //Act
            await repository.DeleteAsync(fileToDelete.Id);
            //Assert
            mock.Verify(r => r.Set<FileData>().Remove(fileToDelete));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task FileDbRepository_GetByIdAsync_ReturnsFile(int numberOfFile)
        {
            //Arrange
            FileData expected = GetTestFiles()[numberOfFile];
            mock.Setup(repo => repo.Set<FileData>().FindAsync(expected.Id).Result).Returns(expected);
            FileDbRepository repository = new(mock.Object);
            //Act
            FileData actual = await repository.GetByIdAsync(expected.Id);
            //Assert
            Assert.Equal(expected, actual);
        }

        private static List<FileData> GetTestFiles()
        {
            var files = new List<FileData>
            {
                new FileData {
                    Id = new Guid("8d52de6c-4b64-41d9-b9e9-1edb89f47def"),
                    Name = "TestFile1.jpg",
                    DateOfUploading = new DateTime(2022, 1, 1),
                    Description = "Test file1 description",
                    IsPrivate = "No",
                    Path = @"C:\Users\miron\OneDrive\Desktop\Final Project\DAL\Files\TestUserName1\TestFile1.jpg",
                    UserId = new Guid("14a49ef4-ba0d-46c2-8abb-fd342c92abf7"),
                    UserName = "TestUserName1",
                    UserFileName = "UserTestFileName1"
                },
                new FileData {
                    Id = new Guid("2c285d75-a0b7-41b8-92db-ca4fa6064dc3"),
                    Name = "TestFile2.jpg",
                    DateOfUploading = new DateTime(2022, 1, 2),
                    Description = "Test file2 description",
                    IsPrivate = "Yes",
                    Path = @"C:\Users\miron\OneDrive\Desktop\Final Project\DAL\Files\TestUserName2\TestFile2.jpg",
                    UserId = new Guid("c1603b73-d137-4aaa-95fe-21cd25da7660"),
                    UserName = "TestUserName2",
                    UserFileName = "UserTestFileName2"
                },
            };
            return files;
        }
    }
}
