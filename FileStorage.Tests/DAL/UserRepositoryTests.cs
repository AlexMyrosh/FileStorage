﻿using DAL.Context;
using DAL.Enums;
using DAL.Models;
using DAL.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.DAL
{
    public class UserRepositoryTests
    {
        private readonly Mock<IFileStorageContext> mock = new();

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserRepository_AddAsync_ReturnsAddedFile(int numberOfUser)
        {
            //Arrange
            User expected = GetTestUsers()[numberOfUser];
            mock.Setup(repo => repo.Set<User>().Add(expected)).Returns(expected);
            UserRepository repository = new(mock.Object);
            //Act
            User actual = await repository.AddAsync(expected);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserRepository_AddAsync_MethodAdAsyncInvoke(int numberOfUser)
        {
            //Arrange
            User expected = GetTestUsers()[numberOfUser];
            mock.Setup(repo => repo.Set<User>().Add(expected)).Returns(expected);
            UserRepository repository = new(mock.Object);
            //Act
            await repository.AddAsync(expected);
            //Assert
            mock.Verify(r => r.Set<User>().Add(expected));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserRepository_DeleteAsync_MethodInvoked(int numberOfUser)
        {
            //Arrange
            User userToDelete = GetTestUsers()[numberOfUser];
            mock.Setup(repo => repo.Set<User>().Remove(userToDelete)).Returns(userToDelete);
            mock.Setup(repo => repo.Set<User>().FindAsync(userToDelete.Id).Result).Returns(userToDelete);
            UserRepository repository = new(mock.Object);
            //Act
            await repository.DeleteAsync(userToDelete.Id);
            //Assert
            mock.Verify(r => r.Set<User>().Remove(userToDelete));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserRepository_GetByIdAsync_ReturnsUser(int numberOfUser)
        {
            //Arrange
            User expected = GetTestUsers()[numberOfUser];
            mock.Setup(repo => repo.Set<User>().FindAsync(expected.Id).Result).Returns(expected);
            UserRepository repository = new(mock.Object);
            //Act
            User actual = await repository.GetByIdAsync(expected.Id);
            //Assert
            Assert.Equal(expected, actual);
        }

        private static List<User> GetTestUsers()
        {
            var reviews = new List<User>
            {
                new User()
                {
                    Id = new Guid("5ffc2f89-81fa-40b6-b9a7-ccc93453b867"),
                    Username = "UsernameTest1",
                    DateOfBirth = new DateTime(1999,08,21),
                    Email = "test1@gmail.com",
                    BIO = "Test1 BIO",
                    Gender = Gender.Male,
                    Password = "qwerty123",
                    Phone = "+380501234567",
                },
                new User()
                {
                    Id = new Guid("7f3af608-2318-4754-a22a-28e55657a9d5"),
                    Username = "UsernameTest2",
                    DateOfBirth = new DateTime(2000,08,21),
                    Email = "test2@gmail.com",
                    BIO = "Test2 BIO",
                    Gender = Gender.Female,
                    Password = "qwerty321",
                    Phone = "+380507654321",
                }
            };
            return reviews;
        }
    }
}
