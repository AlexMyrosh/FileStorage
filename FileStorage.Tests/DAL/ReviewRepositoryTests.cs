﻿using DAL.Context;
using DAL.Models;
using DAL.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.DAL
{
    public class ReviewRepositoryTests
    {
        private readonly Mock<IFileStorageContext> mock = new();

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task ReviewRepository_AddAsyncReview_ReturnsAddedReview(int numberOfReview)
        {
            //Arrange
            Review expected = GetTestReviews()[numberOfReview];
            mock.Setup(repo => repo.Set<Review>().Add(expected)).Returns(expected);
            ReviewRepository repository = new(mock.Object);
            //Act
            Review actual = await repository.AddAsync(expected);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task ReviewRepository_AddAsync_MethodAddAsyncInvoke(int numberOfReview)
        {
            //Arrange
            Review expected = GetTestReviews()[numberOfReview];
            mock.Setup(repo => repo.Set<Review>().Add(expected)).Returns(expected);
            ReviewRepository repository = new(mock.Object);
            //Act
            await repository.AddAsync(expected);
            //Assert
            mock.Verify(r => r.Set<Review>().Add(expected));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task ReviewRepository_DeleteAsync_MethodDeleteAsyncInvoke(int numberOfReview)
        {
            //Arrange
            Review ReviewToDelete = GetTestReviews()[numberOfReview];
            mock.Setup(repo => repo.Set<Review>().Remove(ReviewToDelete)).Returns(ReviewToDelete);
            mock.Setup(repo => repo.Set<Review>().FindAsync(ReviewToDelete.Id).Result).Returns(ReviewToDelete);
            ReviewRepository repository = new(mock.Object);
            //Act
            await repository.DeleteAsync(ReviewToDelete.Id);
            //Assert
            mock.Verify(r => r.Set<Review>().Remove(ReviewToDelete));
        }

        private static List<Review> GetTestReviews()
        {
            var reviews = new List<Review>
            {
                new Review {
                    Id = new Guid("28943343-4654-4614-8ce5-6570dd517700"),
                    Username = "TestUsername1",
                    DateTime = new DateTime(2022,01,01),
                    Text = "Text test review1",
                    UserId = new Guid("46cb2ddc-2de9-44e4-9649-d477f2760190"),
                },
                new Review {
                    Id = new Guid("193fdeb7-2a37-4dcf-8966-39843bbbf1d8"),
                    Username = "TestUsername2",
                    DateTime = new DateTime(2022,01,02),
                    Text = "Text test review2",
                    UserId = new Guid("c7b59d96-e5e0-4ed0-87c6-b3a3952964a0"),
                }
            };
            return reviews;
        }
    }
}
