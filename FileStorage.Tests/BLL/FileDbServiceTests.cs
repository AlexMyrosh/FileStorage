﻿using BLL.DTO;
using BLL.Services;
using DAL.Models;
using DAL.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.BLL
{
    public class FileDbServiceTests
    {
        private readonly Mock<IUnitOfWork> mock = new();
        [Fact]
        public async Task FileDbService_GetAllAsync_ReturnsFileModels()
        {
            // Arrange
            mock.Setup(repo => repo.FileDbRepository.GetAllAsync().Result).Returns(GetTestFiles());
            FileDbService service = new(mock.Object);
            //Act
            List<FileModel> actual = await service.GetAllAsync();
            //Assert
            Assert.Equal(GetTestFiles().Count, actual.Count);
            Assert.Equal(GetTestFiles()[0].Id, actual[0].Id);
            Assert.Equal(GetTestFiles()[1].Id, actual[1].Id);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task FileDbService_GetByIdAsync_ReturnsFileModel(int numberOfRole)
        {
            //Arrange
            Guid testId = GetTestFiles()[numberOfRole].Id;
            mock.Setup(repo => repo.FileDbRepository.GetByIdAsync(testId).Result).Returns(GetTestFiles()[numberOfRole]);
            FileDbService service = new(mock.Object);
            //Act
            FileModel actual = await service.GetByIdAsync(testId);
            //Assert
            Assert.Equal(GetTestFiles()[numberOfRole].Id, actual.Id);
            Assert.Equal(GetTestFiles()[numberOfRole].Name, actual.Name);
            Assert.Equal(GetTestFiles()[numberOfRole].DateOfUploading, actual.DateOfUploading);
            Assert.Equal(GetTestFiles()[numberOfRole].Description, actual.Description);
            Assert.Equal(GetTestFiles()[numberOfRole].IsPrivate, actual.IsPrivate);
            Assert.Equal(GetTestFiles()[numberOfRole].Path, actual.Path);
            Assert.Equal(GetTestFiles()[numberOfRole].UserId, actual.UserId);
            Assert.Equal(GetTestFiles()[numberOfRole].UserName, actual.UserName);
        }
        [Fact]
        public void FileDbService_AddAsync_ThrowsArgumentNullExceptionWithNullFileModel()
        {
            //Arrange
            FileModel fileToAdd = null;
            FileDbService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.AddAsync(fileToAdd));
        }
        [Fact]
        public void FileDbService_DeleteAsync_ThrowsArgumentExceptionWithEmptyId()
        {
            //Arrange
            Guid fileId = Guid.Empty;
            FileDbService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentException>(() => service.DeleteAsync(fileId));
        }
        [Fact]
        public void FileDbService_GetByIdAsync_ThrowsArgumentExceptionWithEmptyId()
        {
            //Arrange
            Guid testId = Guid.Empty;
            FileDbService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(testId));
        }

        private static List<FileData> GetTestFiles()
        {
            var files = new List<FileData>
            {
                new FileData {
                    Id = new Guid("8d52de6c-4b64-41d9-b9e9-1edb89f47def"),
                    Name = "TestFile1.jpg",
                    DateOfUploading = new DateTime(2022, 1, 1),
                    Description = "Test file1 description",
                    IsPrivate = "No",
                    Path = @"C:\Users\miron\OneDrive\Desktop\Final Project\DAL\Files\TestUserName1\TestFile1.jpg",
                    UserId = new Guid("14a49ef4-ba0d-46c2-8abb-fd342c92abf7"),
                    UserName = "TestUserName1",
                    UserFileName = "UserTestFileName1"
                },
                new FileData {
                    Id = new Guid("2c285d75-a0b7-41b8-92db-ca4fa6064dc3"),
                    Name = "TestFile2.jpg",
                    DateOfUploading = new DateTime(2022, 1, 2),
                    Description = "Test file2 description",
                    IsPrivate = "Yes",
                    Path = @"C:\Users\miron\OneDrive\Desktop\Final Project\DAL\Files\TestUserName2\TestFile2.jpg",
                    UserId = new Guid("c1603b73-d137-4aaa-95fe-21cd25da7660"),
                    UserName = "TestUserName2",
                    UserFileName = "UserTestFileName2"
                },
            };
            return files;
        }
    }
}
