﻿using BLL.DTO;
using BLL.Services;
using DAL.Enums;
using DAL.Models;
using DAL.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.BLL
{
    public class UserServiceTests
    {
        private readonly Mock<IUnitOfWork> mock = new();
        [Fact]
        public async Task UserService_GetAllAsync_ReturnsUserModels()
        {
            // Arrange
            mock.Setup(repo => repo.UserRepository.GetAllAsync().Result).Returns(GetTestUsers());
            UserService service = new(mock.Object);
            //Act
            List<UserModel> actual = await service.GetAllAsync();
            //Assert
            Assert.Equal(GetTestUsers().Count, actual.Count);
            Assert.Equal(GetTestUsers()[0].Id, actual[0].Id);
            Assert.Equal(GetTestUsers()[1].Id, actual[1].Id);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserService_GetByIdAsync_ReturnsUserModel(int numberOfUser)
        {
            //Arrange
            Guid testId = GetTestUsers()[numberOfUser].Id;
            mock.Setup(repo => repo.UserRepository.GetByIdAsync(testId).Result).Returns(GetTestUsers()[numberOfUser]);
            UserService service = new(mock.Object);
            //Act
            UserModel actual = await service.GetByIdAsync(testId);
            //Assert
            Assert.Equal(GetTestUsers()[numberOfUser].Id, actual.Id);
            Assert.Equal(GetTestUsers()[numberOfUser].Username, actual.Username);
            Assert.Equal(GetTestUsers()[numberOfUser].DateOfBirth, actual.DateOfBirth);
            Assert.Equal(GetTestUsers()[numberOfUser].Email, actual.Email);
            Assert.Equal(GetTestUsers()[numberOfUser].BIO, actual.BIO);
            Assert.Equal(GetTestUsers()[numberOfUser].Gender.ToString(), actual.Gender.ToString());
            Assert.Equal(GetTestUsers()[numberOfUser].Password, actual.Password);
            Assert.Equal(GetTestUsers()[numberOfUser].Phone, actual.Phone);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task UserService_GetByUsernameAsync_ReturnsUserModel(int numberOfUser)
        {
            //Arrange
            string testUsername = GetTestUsers()[numberOfUser].Username;
            mock.Setup(repo => repo.UserRepository.GetByUsernameAsync(testUsername).Result).Returns(GetTestUsers()[numberOfUser]);
            UserService service = new(mock.Object);
            //Act
            UserModel actual = await service.GetByUsernameAsync(testUsername);
            //Assert
            Assert.Equal(GetTestUsers()[numberOfUser].Id, actual.Id);
            Assert.Equal(GetTestUsers()[numberOfUser].Username, actual.Username);
            Assert.Equal(GetTestUsers()[numberOfUser].DateOfBirth, actual.DateOfBirth);
            Assert.Equal(GetTestUsers()[numberOfUser].Email, actual.Email);
            Assert.Equal(GetTestUsers()[numberOfUser].BIO, actual.BIO);
            Assert.Equal(GetTestUsers()[numberOfUser].Gender.ToString(), actual.Gender.ToString());
            Assert.Equal(GetTestUsers()[numberOfUser].Password, actual.Password);
            Assert.Equal(GetTestUsers()[numberOfUser].Phone, actual.Phone);
        }

        [Fact]
        public void UserService_AddAsync_ThrowsArgumentNullExceptionWithNullUserModel()
        {
            //Arrange
            UserModel userToAdd = null;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.AddAsync(userToAdd));
        }
        [Fact]
        public void UserService_DeleteAsync_ThrowsArgumentExceptionWithEmptyId()
        {
            //Arrange
            Guid Id = Guid.Empty;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.DeleteAsync(Id));
        }
        [Fact]
        public void UserService_GetByIdAsync_ThrowsArgumentExceptionWithEmptyId()
        {
            //Arrange
            Guid testId = Guid.Empty;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(testId));
        }
        [Fact]
        public void UserService_GetByUsernameAsync_ThrowsArgumentExceptionWithEmptyUsername()
        {
            //Arrange
            string testUsername = string.Empty;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByUsernameAsync(testUsername));
        }
        [Fact]
        public void UserService_GetByUsernameAsync_ThrowsArgumentNullExceptionWithNullUsername()
        {
            //Arrange
            string testUsername = null;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.GetByUsernameAsync(testUsername));
        }
        [Fact]
        public void UserService_UpdateAsync_ThrowsArgumentNullExceptionWithNullUserModel()
        {
            //Arrange
            UserModel testUser = null;
            UserService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.UpdateAsync(testUser));
        }
        private static List<User> GetTestUsers()
        {
            var reviews = new List<User>
            {
                new User()
                {
                    Id = new Guid("5ffc2f89-81fa-40b6-b9a7-ccc93453b867"),
                    Username = "UsernameTest1",
                    DateOfBirth = new DateTime(1999,08,21),
                    Email = "test1@gmail.com",
                    BIO = "Test1 BIO",
                    Gender = Gender.Male,
                    Password = "qwerty123",
                    Phone = "+380501234567",
                },
                new User()
                {
                    Id = new Guid("7f3af608-2318-4754-a22a-28e55657a9d5"),
                    Username = "UsernameTest2",
                    DateOfBirth = new DateTime(2000,08,21),
                    Email = "test2@gmail.com",
                    BIO = "Test2 BIO",
                    Gender = Gender.Female,
                    Password = "qwerty321",
                    Phone = "+380507654321",
                }
            };
            return reviews;
        }
    }
}
