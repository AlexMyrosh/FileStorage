﻿using BLL.DTO;
using BLL.Services;
using DAL.Models;
using DAL.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.BLL
{
    public class RoleServiceTests
    {
        private readonly Mock<IUnitOfWork> mock = new();
        [Fact]
        public async Task RoleService_GetAllAsync_ReturnsRoleModels()
        {
            // Arrange
            mock.Setup(repo => repo.RoleRepository.GetAllAsync().Result).Returns(GetTestRoles());
            RoleService service = new(mock.Object);
            //Act
            List<RoleModel> actual = await service.GetAllAsync();
            //Assert
            Assert.Equal(GetTestRoles().Count, actual.Count);
            Assert.Equal(GetTestRoles()[0].Id, actual[0].Id);
            Assert.Equal(GetTestRoles()[1].Id, actual[1].Id);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public async Task RoleService_GetByIdAsync_ReturnsRoleModel(int numberOfRole)
        {
            //Arrange
            Guid testId = GetTestRoles()[numberOfRole].Id;
            mock.Setup(repo => repo.RoleRepository.GetByIdAsync(testId).Result).Returns(GetTestRoles()[numberOfRole]);
            RoleService service = new(mock.Object);
            //Act
            RoleModel actual = await service.GetByIdAsync(testId);
            //Assert
            Assert.Equal(GetTestRoles()[numberOfRole].Id, actual.Id);
            Assert.Equal(GetTestRoles()[numberOfRole].Name, actual.Name);
        }

        [Fact]
        public void RoleService_GetByIdAsync_ThrowsArgumentExceptionWithEmptyId()
        {
            //Arrange
            Guid testId = Guid.Empty;
            RoleService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentException>(() => service.GetByIdAsync(testId));
        }

        private static List<Role> GetTestRoles()
        {
            var users = new List<Role>
            {
                new Role { Id = new Guid("8d52de6c-4b64-41d9-b9e9-1edb89f47def"), Name = "user"},
                new Role { Id = new Guid("e61cbb15-18df-456b-a3b8-ca914058c6c8"), Name = "admin"}
            };
            return users;
        }
    }
}
