﻿using BLL.DTO;
using BLL.Services;
using DAL.Models;
using DAL.UnitOfWork;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace FileStorage.Tests.BLL
{
    public class ReviewServiceTests
    {
        private readonly Mock<IUnitOfWork> mock = new();
        [Fact]
        public async Task ReviewService_GetAllAsync_ReturnsReviewModels()
        {
            // Arrange
            mock.Setup(repo => repo.ReviewRepository.GetAllAsync().Result).Returns(GetTestReviews());
            ReviewService service = new(mock.Object);
            //Act
            List<ReviewModel> actual = await service.GetAllAsync();
            //Assert
            Assert.Equal(GetTestReviews().Count, actual.Count);
            Assert.Equal(GetTestReviews()[0].Id, actual[0].Id);
            Assert.Equal(GetTestReviews()[1].Id, actual[1].Id);
        }
        [Fact]
        public void ReviewService_AddAsync_ThrowsArgumentNullExceptionWithNullReview()
        {
            //Arrange
            ReviewModel reviewToAdd = null;
            ReviewService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.AddAsync(reviewToAdd));
        }
        [Fact]
        public void ReviewService_DeleteAsync_ThrowsArgumentNullExceptionWithNullReview()
        {
            //Arrange
            ReviewModel reviewToDelete = null;
            ReviewService service = new(mock.Object);
            //Assert
            Assert.ThrowsAsync<ArgumentNullException>(() => service.DeleteAsync(reviewToDelete.Id));
        }
        private static List<Review> GetTestReviews()
        {
            var reviews = new List<Review>
            {
                new Review {
                    Id = new Guid("28943343-4654-4614-8ce5-6570dd517700"),
                    Username = "TestUsername1",
                    DateTime = new DateTime(2022,01,01),
                    Text = "Text test review1",
                    UserId = new Guid("46cb2ddc-2de9-44e4-9649-d477f2760190"),
                },
                new Review {
                    Id = new Guid("193fdeb7-2a37-4dcf-8966-39843bbbf1d8"),
                    Username = "TestUsername2",
                    DateTime = new DateTime(2022,01,02),
                    Text = "Text test review2",
                    UserId = new Guid("c7b59d96-e5e0-4ed0-87c6-b3a3952964a0"),
                }
            };
            return reviews;
        }
    }
}
