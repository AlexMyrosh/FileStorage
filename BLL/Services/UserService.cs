﻿using AutoMapper;
using BLL.DTO;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BLL.Services
{
    /// <summary>
    /// Service for working with users in database
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor that initialize default values
        /// </summary>
        public UserService():this(new UnitOfWork())
        {
        }
        /// <summary>
        /// Constructor that initialize unitofWork by parameter
        /// </summary>
        /// <param name="unitOfWork">Element of UnitOfWork</param>
        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }
        /// <summary>
        /// Adds user to database
        /// </summary>
        /// <param name="userModel">User that need to add</param>
        /// <returns>The user</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="userModel"/> is null</exception>
        public Task<UserModel> AddAsync(UserModel userModel)
        {
            if (userModel == null) throw new ArgumentNullException("userModel", "User must be not null");
            return this.AddInternalAsync(userModel);
        }
        private async Task<UserModel> AddInternalAsync(UserModel userModel)
        {
            var mappedModel = _mapper.Map<UserModel, User>(userModel);
            var unmappedModel = await _unitOfWork.UserRepository.AddAsync(mappedModel);
            return _mapper.Map<User, UserModel>(unmappedModel);
        }
        /// <summary>
        /// Returns all users from database
        /// </summary>
        /// <returns>All users from database</returns>
        public async Task<List<UserModel>> GetAllAsync()
        {
            var unmappedModel = await _unitOfWork.UserRepository.GetAllAsync();
            var mappedModel = _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(unmappedModel);
            return mappedModel.ToList();
        }
        /// <summary>
        /// Delete user with <paramref name="Id"/> from database
        /// </summary>
        /// <param name="Id">Id of user that need to delete</param>
        /// <returns>Represents an asynchronus operation</returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task DeleteAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("User Id must be not empty");
            return this.DeleteInternalAsync(Id);
        }
        private async Task DeleteInternalAsync(Guid Id)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(Id);
            await _unitOfWork.ReviewRepository.MarkReviewsAsDeleted(user.Reviews);
            _unitOfWork.FileServerRepository.DeleteFolderWithFiles(user.Username);

             await _unitOfWork.UserRepository.DeleteAsync(Id);
        }
        /// <summary>
        /// Updates information about user in database
        /// </summary>
        /// <param name="newUserModel">User with new data but the same Id</param>
        /// <returns>Updated user</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="newUserModel"/> is null</exception>
        public Task<UserModel> UpdateAsync(UserModel newUserModel)
        {
            if (newUserModel == null) throw new ArgumentNullException("newUserModel", "User must be not null");
            return this.UpdateInternalAsync(newUserModel);
        }
        private async Task<UserModel> UpdateInternalAsync(UserModel newUserModel)
        {
            if (newUserModel.Username != null)
            {
                User oldUser = (await _unitOfWork.UserRepository.GetAllAsync())
                .FirstOrDefault(x => x.Username == HttpContext.Current.User.Identity.Name);

                _unitOfWork.FileServerRepository.Update(oldUser.Username, newUserModel.Username);

                var files = oldUser.Files;
                if (files.Count != 0)
                {
                    foreach (var file in files)
                    {
                        string newPath = file.Path.Replace(oldUser.Username, newUserModel.Username);
                        file.Path = newPath;
                        file.UserName = newUserModel.Username;
                        await _unitOfWork.FileDbRepository.UpdateAsync(file);
                    }
                }

                var reviews = oldUser.Reviews;
                if(reviews.Count != 0)
                {
                    await _unitOfWork.ReviewRepository.UpdateUsername(reviews, newUserModel.Username);
                }

            }

            var mappedModel = _mapper.Map<UserModel, User>(newUserModel);
            var unmappedResult =  await _unitOfWork.UserRepository.UpdateAsync(mappedModel);
            return _mapper.Map<User, UserModel>(unmappedResult);
        }
        /// <summary>
        /// Returns concrete user from database
        /// </summary>
        /// <param name="Id">Id of user that need to get</param>
        /// <returns>User with Id = <paramref name="Id"/></returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task<UserModel> GetByIdAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("User id must be not empty");
            return this.GetByIdInternalAsync(Id);
        }
        private async Task<UserModel> GetByIdInternalAsync(Guid Id)
        {
            var unmapped = await _unitOfWork.UserRepository.GetByIdAsync(Id);
            return _mapper.Map<User, UserModel>(unmapped);
        }
        /// <summary>
        /// Returns concrete user from database
        /// </summary>
        /// <param name="username">Username of user that need to get</param>
        /// <returns>User with username = <paramref name="username"/></returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/> is empty</exception>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="username"/> is null</exception>
        public Task<UserModel> GetByUsernameAsync(string username)
        {
            if (username == String.Empty) throw new ArgumentException("Username must be not empty");
            if (username == null) throw new ArgumentNullException("username", "Username must be null");
            return this.GetByUsernameInternaAsync(username);
        }
        private async Task<UserModel> GetByUsernameInternaAsync(string username)
        {
            var unmapped = await _unitOfWork.UserRepository.GetByUsernameAsync(username);
            return _mapper.Map<User, UserModel>(unmapped);
        }
    }
}
