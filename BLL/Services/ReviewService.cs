﻿using AutoMapper;
using BLL.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using DAL.Models;
using BLL.DTO;
using DAL.UnitOfWork;

namespace BLL.Services
{
    /// <summary>
    /// Service for working with reviews in database
    /// </summary>
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor that initialize default values
        /// </summary>
        public ReviewService():this(new UnitOfWork())
        {
        }
        /// <summary>
        /// Constructor that initialize unitofWork by parameter
        /// </summary>
        /// <param name="unitOfWork">Element of UnitOfWork</param>
        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }
        /// <summary>
        /// Returns all reviews from database
        /// </summary>
        /// <returns>All reviews from database</returns>
        public async Task<List<ReviewModel>> GetAllAsync()
        {
            var unmappedModel = await _unitOfWork.ReviewRepository.GetAllAsync();
            var mappedModel = _mapper.Map<IEnumerable<Review>, IEnumerable<ReviewModel>>(unmappedModel);
            return mappedModel.ToList();
        }
        /// <summary>
        /// Adds review to database
        /// </summary>
        /// <param name="reviewModel">Review that need to add</param>
        /// <returns>The review</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="reviewModel"/> is null</exception>
        public Task<ReviewModel> AddAsync(ReviewModel reviewModel)
        {
            if (reviewModel == null) throw new ArgumentNullException("reviewModel", "Review must be not null");
            return this.AddInternalAsync(reviewModel);
        }
        private async Task<ReviewModel> AddInternalAsync(ReviewModel reviewModel)
        {
            var mappedModel = _mapper.Map<ReviewModel, Review>(reviewModel);
            var unmappedResult = await _unitOfWork.ReviewRepository.AddAsync(mappedModel);
            return _mapper.Map<Review, ReviewModel>(unmappedResult);
        }
        /// <summary>
        /// Delete review from database
        /// </summary>
        /// <param name="Id">Id of review to delete</param>
        /// <returns>Represents an asynchronus operation</returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task DeleteAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("Review id must not be empty");
            return this.DeleteInternalAsync(Id);
        }
        private async Task DeleteInternalAsync(Guid Id)
        {
            await _unitOfWork.ReviewRepository.DeleteAsync(Id);
        }
    }
}
