﻿using AutoMapper;
using BLL.DTO;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for working with roles in database
    /// </summary>
    public class RoleService:IRoleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor that initialize default values
        /// </summary>
        public RoleService():this(new UnitOfWork())
        {
        }
        /// <summary>
        /// Constructor that initialize unitofWork by parameter
        /// </summary>
        /// <param name="unitOfWork">Element of UnitOfWork</param>
        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }
        /// <summary>
        /// Returns all roles from database
        /// </summary>
        /// <returns>All roles from database</returns>
        public async Task<List<RoleModel>> GetAllAsync()
        {
            var unmappedModel = await _unitOfWork.RoleRepository.GetAllAsync();
            var mappedModel = _mapper.Map<IEnumerable<Role>, IEnumerable<RoleModel>>(unmappedModel);
            return mappedModel.ToList();
        }
        /// <summary>
        /// Returns concrete role from database
        /// </summary>
        /// <param name="Id">Id of role that need to get</param>
        /// <returns>Role with Id = <paramref name="Id"/></returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task<RoleModel> GetByIdAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("Role id must be not empty");

            return this.GetByIdInternalAsync(Id);
        }
        private async Task<RoleModel> GetByIdInternalAsync(Guid Id)
        {
            var unmappedModel = await _unitOfWork.RoleRepository.GetByIdAsync(Id);
            return _mapper.Map<Role, RoleModel>(unmappedModel);
        }
    }
}
