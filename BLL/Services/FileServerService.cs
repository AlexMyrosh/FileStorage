﻿using BLL.Services.Interfaces;
using DAL.UnitOfWork;
using System;
using System.Web;

namespace BLL.Services
{
    /// <summary>
    /// Service for working with files on server
    /// </summary>
    public class FileServerService : IFileServerService
    {
        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// Constructor that initialize default values
        /// </summary>
        public FileServerService():this(new UnitOfWork())
        {
            
        }
        /// <summary>
        /// Constructor that initialize unitofWork by parameter
        /// </summary>
        /// <param name="unitOfWork">Element of UnitOfWork</param>
        public FileServerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Uploads file on server
        /// </summary>
        /// <param name="upload">Provide access to individual files</param>
        /// <param name="UserName">Name of file`s owner</param>
        /// <param name="FileName">Name of file</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="upload"/> is null or 
        /// <paramref name="UserName"/> or <paramref name="FileName"/> is null or empty</exception>
        public void UploadFile(HttpPostedFileBase upload, string UserName, string FileName)
        {
            if (upload == null) throw new ArgumentNullException("upload", "HttpPosterdFile - 'upload', must be not null");
            if (UserName == null || UserName == String.Empty) throw new ArgumentNullException("UserName", "UserName must be not null");
            if (FileName == null || FileName == String.Empty) throw new ArgumentNullException("FileName", "FileName must be not null");

            _unitOfWork.FileServerRepository.Add(upload, UserName, FileName);
        }
        /// <summary>
        /// Delete file from server
        /// </summary>
        /// <param name="path">path on server where neccessary file is keeping</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> is null or empty</exception>
        public void DeleteFile(string path)
        {
            if (path == null || path == String.Empty) throw new ArgumentNullException("path", "Path to file must be not null");
            _unitOfWork.FileServerRepository.Delete(path);
        }
        /// <summary>
        /// Change folder`s name on server
        /// </summary>
        /// <param name="oldUsername">old folder name</param>
        /// <param name="newUsername">new folder name</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="oldUsername"/> or <paramref name="newUsername"/> is null or empty</exception>
        public void UpdateFolderName(string oldUsername, string newUsername)
        {
            if (oldUsername == null || oldUsername == String.Empty) throw new ArgumentNullException("oldUsername", "oldUsername must be not null");
            if (newUsername == null || newUsername == String.Empty) throw new ArgumentNullException("newUsername", "newUsername must be not null");

            _unitOfWork.FileServerRepository.Update(oldUsername, newUsername);
        }
    }
}
