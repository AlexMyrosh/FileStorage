﻿using AutoMapper;
using BLL.DTO;
using BLL.Services.Interfaces;
using DAL.Models;
using DAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for working with files in database
    /// </summary>
    public class FileDbService : IFileDbService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        /// <summary>
        /// Constructor that initialize default values
        /// </summary>
        public FileDbService() : this(new UnitOfWork())
        {
            
        }
        /// <summary>
        /// Constructor that initialize unitofWork by parameter
        /// </summary>
        /// <param name="unitOfWork">Element of UnitOfWork</param>
        public FileDbService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));
            _mapper = new Mapper(configuration);
        }
        /// <summary>
        /// Adds file to database
        /// </summary>
        /// <param name="fileModel">File that need to add</param>
        /// <returns>The file</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="fileModel"/> is null</exception>
        public Task<FileModel> AddAsync(FileModel fileModel)
        {
            if (fileModel == null) throw new ArgumentNullException("fileModel", "File must be not null");

            return this.AddInternalAsync(fileModel);
        }
        private async Task<FileModel> AddInternalAsync(FileModel fileModel)
        {
            var fileMapped = _mapper.Map<FileModel, FileData>(fileModel);
            var unmappedResult =  await _unitOfWork.FileDbRepository.AddAsync(fileMapped);
            return _mapper.Map<FileData, FileModel>(unmappedResult);
        }
        /// <summary>
        /// Returns all files from database
        /// </summary>
        /// <returns>All files from database</returns>
        public async Task<List<FileModel>> GetAllAsync()
        {
            var unmappedModel = await _unitOfWork.FileDbRepository.GetAllAsync();
            var mappedModel = _mapper.Map<IEnumerable<FileData>, IEnumerable<FileModel>>(unmappedModel);
            return mappedModel.ToList();
        }
        /// <summary>
        /// Delete file from database
        /// </summary>
        /// <param name="Id">Id of file to delete</param>
        /// <returns>Represents an asynchronus operation</returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task DeleteAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("File Id must be not empty");
            return this.DeleteInternalAsync(Id);
        }
        private async Task DeleteInternalAsync(Guid Id)
        {
            await _unitOfWork.FileDbRepository.DeleteAsync(Id);
        }
        /// <summary>
        /// Returns concrete file from database
        /// </summary>
        /// <param name="Id">Id of file that need to get</param>
        /// <returns>File with Id = <paramref name="Id"/></returns>
        /// <exception cref="ArgumentException">Thrown when <paramref name="Id"/> is empty</exception>
        public Task<FileModel> GetByIdAsync(Guid Id)
        {
            if (Id == Guid.Empty) throw new ArgumentException("File id must be not empty");
            return this.GetByIdInternalAsync(Id);
        }
        private async Task<FileModel> GetByIdInternalAsync(Guid Id)
        {
            var unmappedModel = await _unitOfWork.FileDbRepository.GetByIdAsync(Id);
            return _mapper.Map<FileData, FileModel>(unmappedModel);
        }
    }
}
