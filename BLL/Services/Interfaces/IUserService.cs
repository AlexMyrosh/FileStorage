﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Provides a mechanism for working with users in database
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Adds user to database
        /// </summary>
        /// <param name="userModel">User that will be added</param>
        /// <returns>Id of added user</returns>
        Task<UserModel> AddAsync(UserModel userModel);
        /// <summary>
        /// Returns all the users from database
        /// </summary>
        /// <returns>List of all users</returns>
        Task<List<UserModel>> GetAllAsync();
        /// <summary>
        /// Deletes user from database, all the files of this user from database 
        /// and marked reviews of this user from database as from deleted user
        /// </summary>
        /// <param name="Id">Id of user that need to delete</param>
        /// <returns>True if deleting was succesful, otherwise - false</returns>
        Task DeleteAsync(Guid Id);
        /// <summary>
        /// Updates user information and name of folder on server where user`s files is storing
        /// </summary>
        /// <param name="newUserModel">New user information</param>
        /// <returns>True if updating was succesful, otherwise - false</returns>
        Task<UserModel> UpdateAsync(UserModel newUserModel);
        /// <summary>
        /// Returns user by Id
        /// </summary>
        /// <param name="Id">Id of user that need to return</param>
        /// <returns>User with the Id</returns>
        Task<UserModel> GetByIdAsync(Guid Id);
        /// <summary>
        /// Returns user by username
        /// </summary>
        /// <param name="username">Username of user that need to return</param>
        /// <returns>User with the username</returns>
        Task<UserModel> GetByUsernameAsync(string username);
    }
}
