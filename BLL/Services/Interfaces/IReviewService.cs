﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Provides a mechanism for working with reviews in database
    /// </summary>
    public interface IReviewService
    {
        /// <summary>
        /// Adds review to database
        /// </summary>
        /// <param name="reviewModel">Review that need to add</param>
        /// <returns>If of added review</returns>
        Task<ReviewModel> AddAsync(ReviewModel reviewModel);
        /// <summary>
        /// Returns all the reviews of all the users from database
        /// </summary>
        /// <returns>List of reviews of all users</returns>
        Task<List<ReviewModel>> GetAllAsync();
        /// <summary>
        /// Deletes review from database
        /// </summary>
        /// <param name="Id">Id of review that need to delete</param>
        /// <returns>True if deleting was succesful, otherwise - false</returns>
        Task DeleteAsync(Guid Id);
    }
}
