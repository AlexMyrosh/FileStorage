﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Provides a mechanism for working with files in database
    /// </summary>
    public interface IFileDbService
    {
        /// <summary>
        /// Adds file to database
        /// </summary>
        /// <param name="fileModel">File that will be added</param>
        /// <returns>Id of added file</returns>
        Task<FileModel> AddAsync(FileModel fileModel);
        /// <summary>
        /// Returns all the files of all the users from database
        /// </summary>
        /// <returns>List of files of all users</returns>
        Task<List<FileModel>> GetAllAsync();
        /// <summary>
        /// Deletes file from database
        /// </summary>
        /// <param name="Id">Id of file that need to delete</param>
        /// <returns>True if deleting was succesful, otherwise - false</returns>
        Task DeleteAsync(Guid Id);
        /// <summary>
        /// Returns file by Id
        /// </summary>
        /// <param name="Id">Id of file that need to return</param>
        /// <returns>File with the Id</returns>
        Task<FileModel> GetByIdAsync(Guid Id);
    }
}
