﻿using System.Web;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Provides a mechanism for working with files on server
    /// </summary>
    public interface IFileServerService
    {
        /// <summary>
        /// Uploads file on server
        /// </summary>
        /// <param name="upload">Helps to upload file on server</param>
        /// <param name="UserName">Name of user`s folder, where file will be uploaded</param>
        /// <param name="FileName">Name of file that will be uploaded</param>
        void UploadFile(HttpPostedFileBase upload, string UserName, string FileName);
        /// <summary>
        /// Delets file from server
        /// </summary>
        /// <param name="path">Path where the file is storing</param>
        void DeleteFile(string path);
        /// <summary>
        /// Change name of folder where user`s file is keeping
        /// </summary>
        /// <param name="oldUsername">old name of folder</param>
        /// <param name="newUsername">new name of folder</param>
        void UpdateFolderName(string oldUsername, string newUsername);
    }
}
