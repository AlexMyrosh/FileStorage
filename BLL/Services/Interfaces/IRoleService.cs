﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services.Interfaces
{
    /// <summary>
    /// Provides a mechanism for working with roles in database
    /// </summary>
    public interface IRoleService
    {
        /// <summary>
        /// Returns all the roles from database
        /// </summary>
        /// <returns>List of all roles</returns>
        Task<List<RoleModel>> GetAllAsync();
        /// <summary>
        /// Returns role by Id
        /// </summary>
        /// <param name="Id">Id of role that need to return</param>
        /// <returns>Role with the Id</returns>
        Task<RoleModel> GetByIdAsync(Guid Id);
    }
}
