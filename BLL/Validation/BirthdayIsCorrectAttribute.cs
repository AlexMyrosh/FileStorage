﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.Validation
{
    /// <summary>
    /// Validation attribute for DataTime type of birthday
    /// </summary>
    
    public sealed class BirthdayIsCorrectAttribute : ValidationAttribute
    {
        ///<inheritdoc/>
        public override bool IsValid(object value)
        {
            if (value == null) return true;
            DateTime date = (DateTime)value;
            return (date > new DateTime(1900,1,1) && date < DateTime.Now);
        }
    }
}
