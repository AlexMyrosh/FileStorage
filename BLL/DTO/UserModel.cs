﻿using BLL.Enums;
using BLL.Validation;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    /// <summary>
    /// Model of already registered user
    /// </summary>
    public class UserModel
    {
        /// <summary>
        /// Id of user
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Original username of user
        /// </summary>
        [MaxLength(20, ErrorMessage = "Try something smaller")]
        public string Username { get; set; }
        /// <summary>
        /// Original email of user
        /// </summary>
        [RegularExpression(@"^$|[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Incorrect email")]
        public string Email { get; set; }
        /// <summary>
        /// Password from account
        /// </summary>
        [DataType(DataType.Password)]
        [RegularExpression(@"^$|(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}", ErrorMessage = "Password must have minimum 8 characters, at least 1 letter and 1 number")]
        public string Password { get; set; }
        /// <summary>
        /// User`s phone
        /// </summary>
        [RegularExpression(@"^$|(\+?[0-9\s\-\(\)]+)", ErrorMessage = "Phone has incorrect format")]
        [MaxLength(20, ErrorMessage = "Phone must contain maximum 20 numbers")]
        [MinLength(5, ErrorMessage = "Phone must contain minimum 5 numbers")]
        public string Phone { get; set; }
        /// <summary>
        /// Some text about user
        /// </summary>
        public string BIO { get; set; }
        /// <summary>
        /// Date of birthday of user
        /// </summary>
        [BirthdayIsCorrect(ErrorMessage ="Date is incorrect")]
        [DataType(DataType.Date)]
        [Display(Name = "Birthday")]
        public DateTime? DateOfBirth { get; set; }
        /// <summary>
        /// Gender of user
        /// </summary>
        public GenderModel? Gender { get; set; }
        /// <summary>
        /// Id of role of user
        /// </summary>
        public Guid RoleId { get; set; }
        /// <summary>
        /// All users files
        /// </summary>
        public virtual ICollection<FileModel> Files { get; set; }
        /// <summary>
        /// All users reviews
        /// </summary>
        public virtual ICollection<ReviewModel> Reviews { get; set; }
    }
}
