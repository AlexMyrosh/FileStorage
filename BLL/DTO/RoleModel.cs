﻿using System;

namespace BLL.DTO
{
    /// <summary>
    /// Model of role of user to have different access
    /// </summary>
    public class RoleModel
    {
        /// <summary>
        /// Id of role
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Name of role of user
        /// </summary>
        public string Name { get; set; }
    }
}
