﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    /// <summary>
    /// Model of file that user upload on the server
    /// </summary>
    public class FileModel
    {
        /// <summary>
        /// Id of file
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Original name of file that user upload
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Custom name of file that user set when upload the file
        /// If user doesn`t set this name, it`ll be equal to name of the file
        /// </summary>
        public string NameSetByUser { get; set; }
        /// <summary>
        /// Description of the file
        /// It`s not necessarily and it can be null
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Date and time when the file was uploaded on server
        /// </summary>
        [Display(Name = "Uploaded at")]
        public DateTime DateOfUploading { get; set; }
        /// <summary>
        /// Path on the server, where exactly file is keeping
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Id of user who uploaded this file
        /// </summary>
        public Guid UserId { get; set; }
        /// <summary>
        /// Username of user who uploaded this files
        /// </summary>
        [Display(Name = "User")]
        public string UserName { get; set; }
        /// <summary>
        /// Privacy of the file
        /// Set only two states: Yes and No
        /// If it`s Yes - everyone can see this file by link, otherwise - only user that uploaded the file
        /// </summary>
        [Display(Name = "Is file private")]
        public string IsPrivate { get; set; }
    }
}
