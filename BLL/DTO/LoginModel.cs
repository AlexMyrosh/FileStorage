﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    /// <summary>
    /// Model to login into personal account
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Email or username of user who has already registered to log in into account
        /// </summary>
        [MinLength(2, ErrorMessage = "Try something larger")]
        [MaxLength(20, ErrorMessage = "Try something smaller")]
        [Required(ErrorMessage = "Your forgot to enter your login")]
        public string Email { get; set; }
        /// <summary>
        /// Password of user who has already registered to log in into account
        /// </summary>
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Your forgot to enter your password")]
        public string Password { get; set; }
    }
}
