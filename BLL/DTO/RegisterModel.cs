﻿using System.ComponentModel.DataAnnotations;

namespace BLL.DTO
{
    /// <summary>
    /// Model to create personal account
    /// </summary>
    public class RegisterModel
    {
        /// <summary>
        /// Username of user that want to be registered
        /// </summary>
        [MinLength(2, ErrorMessage = "Try something larger")]
        [MaxLength(20, ErrorMessage = "Try something smaller")]
        [Required( ErrorMessage = "Your forgot to enter your username")]
        public string Username { get; set; }
        /// <summary>
        /// Email of user that want to be registered
        /// </summary>
        [RegularExpression(@"^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$", ErrorMessage = "Incorrect email")]
        [Required(ErrorMessage = "Your forgot to enter your email")]
        public string Email { get; set; }
        /// <summary>
        /// Password of user account that want to be registered
        /// </summary>

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Your forgot to enter your password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$", ErrorMessage = "Password must have minimum 8 characters, at least 1 letter and 1 number")]
        public string Password { get; set; }
    }
}
