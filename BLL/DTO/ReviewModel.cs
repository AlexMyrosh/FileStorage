﻿using System;

namespace BLL.DTO
{
    /// <summary>
    /// Model of review on web-application
    /// </summary>
    public class ReviewModel
    {
        /// <summary>
        /// Id of review
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Username of person who leaves a review
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Date and time when review was left
        /// </summary>
        public DateTime DateTime { get; set; }
        /// <summary>
        /// Text of review
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Name of role who left review
        /// </summary>
        public string Role { get; set; }
        /// <summary>
        /// Id of user that left the review
        /// </summary>
        public Guid? UserId { get; set; }

    }
}
