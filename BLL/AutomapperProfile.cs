﻿using AutoMapper;
using BLL.DTO;
using DAL.Models;

namespace BLL
{
    /// <summary>
    /// Automapper to convert one models to other
    /// </summary>
    class AutomapperProfile : Profile
    {
        /// <summary>
        /// Describe how to convert Review to ReviewModel and vice versa,
        /// FileData to FileModel and vice versa,
        /// User to UserModel and vice versa,
        /// Role to RoleModel and vice versa,
        /// </summary>
        public AutomapperProfile()
        {
            CreateMap<Review, ReviewModel>()
                .ForMember(r => r.Id, d => d.MapFrom(r => r.Id))
                .ForMember(r => r.Username, d => d.MapFrom(r => r.Username))
                .ForMember(r => r.Text, d => d.MapFrom(r => r.Text))
                .ForMember(r => r.DateTime, d => d.MapFrom(r => r.DateTime))
                .ForMember(r => r.Role, d => d.MapFrom(r => r.Role))
                .ForMember(r => r.UserId, d => d.MapFrom(r => r.UserId))
                .ReverseMap();

            CreateMap<FileData, FileModel>()
                .ForMember(fd => fd.Id, d => d.MapFrom(f => f.Id))
                .ForMember(fd => fd.Name, d => d.MapFrom(f => f.Name))
                .ForMember(fd => fd.NameSetByUser, d => d.MapFrom(f => f.UserFileName))
                .ForMember(fd => fd.Description, d => d.MapFrom(f => f.Description))
                .ForMember(fd => fd.DateOfUploading, d => d.MapFrom(f => f.DateOfUploading))
                .ForMember(fd => fd.Path, d => d.MapFrom(f => f.Path))
                .ForMember(fd => fd.UserId, d => d.MapFrom(f => f.UserId))
                .ForMember(fd => fd.UserName, d => d.MapFrom(f => f.UserName))
                .ForMember(fd => fd.IsPrivate, d => d.MapFrom(f => f.IsPrivate))
                .ReverseMap();

            CreateMap<User, UserModel>()
                .ForMember(ud => ud.Id, d => d.MapFrom(u => u.Id))
                .ForMember(ud => ud.Username, d => d.MapFrom(u => u.Username))
                .ForMember(ud => ud.Email, d => d.MapFrom(u => u.Email))
                .ForMember(ud => ud.Password, d => d.MapFrom(u => u.Password))
                .ForMember(ud => ud.Phone, d => d.MapFrom(u => u.Phone))
                .ForMember(ud => ud.BIO, d => d.MapFrom(u => u.BIO))
                .ForMember(ud => ud.DateOfBirth, d => d.MapFrom(u => u.DateOfBirth))
                .ForMember(ud => ud.Gender, d => d.MapFrom(u => u.Gender))
                .ForMember(ud => ud.RoleId, d => d.MapFrom(u => u.RoleId))
                .ReverseMap();

            CreateMap<Role, RoleModel>()
                .ForMember(rd => rd.Id, d => d.MapFrom(r => r.Id))
                .ForMember(rd => rd.Name, d => d.MapFrom(r => r.Name))
                .ReverseMap();
        }
    }
}
