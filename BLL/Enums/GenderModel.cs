﻿namespace BLL.Enums
{
    /// <summary>
    /// Enum for all possible variants of user`s gender
    /// </summary>
    public enum GenderModel
    {
        /// <summary>
        /// If user is man
        /// </summary>
        Male = 0,
        /// <summary>
        /// If user is woman
        /// </summary>
        Female = 1,
        /// <summary>
        /// if user doesn`t want to tell gender and it`s default value
        /// </summary>
        Other = 2
    }
}
