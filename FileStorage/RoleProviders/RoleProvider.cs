﻿using BLL.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;

namespace FileStorage.RoleProviders
{
    public class MyRoleProvider : RoleProvider
    {
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override string[] GetRolesForUser(string username)
        {
            string[] roles = new string[] { };
            UserService userService = new UserService();
            RoleService roleService = new RoleService();
            var task = Task.Run(async () => {
                return await userService.GetAllAsync();
            });

            var res = task.Result.FirstOrDefault(u => u.Username == username);
            if (res != null)
            {
                var role = Task.Run(async () => {
                    return await roleService.GetAllAsync();
                });
                roles = new string[] { role.Result.FirstOrDefault(r => r.Id == res.RoleId).Name };
            }
            return roles;
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }


        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}