﻿"use strict";

const file = document.querySelector('#file');
file.addEventListener('change', (e) => {
    const [files] = e.target.files;
    // Get the file name and size
    const { name: fileName, size } = files;
    // Convert size in bytes to MB
    let fileSize = (size / 1048576).toFixed(2);
    // Set the text content
    var fileNameAndSize = ""
    if (fileSize >= 1000) {
        fileNameAndSize = `Sorry, but the file is too large (${fileSize}MB). Max size is 1000MB`;
        document.querySelector('#submit').disabled = true;
    }
    else if (fileSize < 1) {
        fileSize = (size / 1024).toFixed(2);
        fileNameAndSize = `${fileName} - ${fileSize}KB`;
        document.querySelector('#submit').disabled = false;
    }
    else {
        fileNameAndSize = `${fileName} - ${fileSize}MB`;
        document.querySelector('#submit').disabled = false;
    }
    document.querySelector('.file-name-and-size').textContent = fileNameAndSize;
    document.querySelector('.file-name-and-size').style.visibility = 'visible';

});