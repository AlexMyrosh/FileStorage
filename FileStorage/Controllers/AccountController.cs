﻿using BLL.DTO;
using BLL.Enums;
using BLL.Services;
using BLL.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

namespace FileStorage.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        public AccountController(IUserService userService, IRoleService roleService)
        {
            _userService = userService;
            _roleService = roleService;
        }
        public AccountController() : this(new UserService(), new RoleService())
        {
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LoginAsync(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserModel user = (await _userService.GetAllAsync())
                    .FirstOrDefault(u => (u.Email == model.Email.ToLower() || u.Username == model.Email) 
                    && u.Password == model.Password);

                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(user.Username, true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "There are may be mistake in login or password");
                }
            }

            return View("Login", model);
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterAsync(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userByEmail = (await _userService.GetAllAsync()).FirstOrDefault(u => u.Email == model.Email.ToLower());
                var userByName = (await _userService.GetByUsernameAsync(model.Username));

                if (userByName != null)
                {
                    ModelState.AddModelError("Username", "User with the same username already exists!");
                }
                if (userByEmail != null)
                {
                    ModelState.AddModelError("Email", "User with the same email already exists!");
                }
            }
            if (ModelState.IsValid) {

                var roleUserId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "user").Id;

                UserModel newUser = new UserModel()
                {
                    Username = model.Username,
                    Email = model.Email.ToLower(),
                    Password = model.Password,
                    RoleId = roleUserId,
                    Gender = GenderModel.Other
                };
                await _userService.AddAsync(newUser);
                FormsAuthentication.SetAuthCookie(model.Username, true);
                return RedirectToAction("Index", "Home");
            }
            return View("Register",model);
        }
        [Authorize]
        public ActionResult Logoff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        [Authorize]
        public ActionResult ChangeData()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeDataAsync(UserModel newUserData)
        {
            UserModel userByEmail = null;
            UserModel userByName = null;
            if (newUserData.Email != null) userByEmail = (await _userService.GetAllAsync()).FirstOrDefault(u => u.Email == newUserData.Email.ToLower());
            if(newUserData.Username != null) userByName = (await _userService.GetByUsernameAsync(newUserData.Username));

            if (userByName != null) ModelState.AddModelError("Username", "User with the same username already exists!");
            if (userByEmail != null) ModelState.AddModelError("Email", "User with the same email already exists!");

            if (ModelState.IsValid && !IsEmptyUserModel(newUserData))
            {
                newUserData.Id = (await _userService.GetByUsernameAsync(this.User.Identity.Name)).Id;
                

                var updatedUser = await _userService.UpdateAsync(newUserData);

                FormsAuthentication.SignOut();
                var logInfo = new LoginModel()
                {
                    Email =  updatedUser.Email,
                    Password = updatedUser.Password
                };
                return await this.LoginAsync(logInfo);
            }
            else
            {
                return View("ChangeData");
            }
        }
        private bool IsEmptyUserModel(UserModel user)
        {
            if (user.BIO == null && user.Username == null && user.Email == null
                && user.Password == null && user.Phone == null
                && user.DateOfBirth == null && user.Gender == null) 
                return true;

            return false;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> ShowDataAsync()
        {
            var currentUser = await _userService.GetByUsernameAsync(this.User.Identity.Name);

            return View("ShowData", currentUser);
        }
        [Authorize]
        public async Task<ActionResult> DeleteCurrentUserAsync()
        {
            var currentUser = await _userService.GetByUsernameAsync(this.User.Identity.Name);
            var adminRoleId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;
            if (currentUser.RoleId == adminRoleId && await IsItLastAdmin())
            {
                ModelState.AddModelError("Deleting", "At least 1 admin must remain!");
                return View("ShowData", currentUser);
            }
            await _userService.DeleteAsync(currentUser.Id);
            return RedirectToAction("Logoff");
        }
        private async Task<bool> IsItLastAdmin()
        {
            var users = await _userService.GetAllAsync();
            var adminRoleId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;
            int count = 0;
            foreach(var user in users)
            {
                if (user.RoleId == adminRoleId) count++;
            }
            return count <= 1;
        }
    }
}