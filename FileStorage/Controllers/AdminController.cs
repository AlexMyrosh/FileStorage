﻿using BLL.DTO;
using BLL.Enums;
using BLL.Services;
using BLL.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FileStorage.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly IUserService _userService;
        private readonly IFileDbService _fileService;
        private readonly IRoleService _roleService;
        private readonly IFileServerService _fileServerService;
        public AdminController(IUserService service, IFileDbService fileService, IRoleService roleService, IFileServerService fileServerService)
        {
            _userService = service;
            _fileService = fileService;
            _roleService = roleService;
            _fileServerService = fileServerService;
        }
        public AdminController() : this(new UserService(), new FileDbService(), new RoleService(), new FileServerService())
        {
        }
        [HttpGet]
        public async Task<ActionResult> ViewAllUsersDataAsync()
        {
            ViewBag.AdminId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;
            var usersToShow = await _userService.GetAllAsync();
            return View("ViewAllUsersData", usersToShow);
        }
        public async Task<ActionResult> DeleteUserAsync(Guid Id)
        {
            await _userService.DeleteAsync(Id);
            return RedirectToAction("ViewAllUsersDataAsync", "Admin");
        }
        public async Task<ActionResult> ViewUserFilesAsync(Guid Id)
        {
            var user = await _userService.GetByIdAsync(Id);
            ViewBag.UserName = user.Username;
            ViewBag.UserId = Id;
            return View("ViewUserFiles", user.Files);
        }
        public async Task<FileResult> DownloadAsync(Guid Id)
        {
            var file = await _fileService.GetByIdAsync(Id);
            byte[] fileBytes = System.IO.File.ReadAllBytes(file.Path);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Name);
        }
        public async Task<ActionResult> DeleteFileFromUserFilesPageAsync(Guid fileId, Guid userId)
        {
            await DeleteFileAsync(fileId, userId);
            return RedirectToAction("ViewUserFilesAsync", new { Id = userId });
        }

        public async Task<ActionResult> DeleteFileFromAllFilesPageAsync(Guid fileId, Guid userId)
        {
            await DeleteFileAsync(fileId, userId);
            return RedirectToAction("ViewAllFilesAsync");

        }

        private async Task DeleteFileAsync(Guid fileId, Guid userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            var path = user.Files.FirstOrDefault(f => f.Id == fileId).Path;
            _fileServerService.DeleteFile(path);

            await _fileService.DeleteAsync(fileId);
        }

        [HttpGet]
        public async Task<ActionResult> ViewAllFilesAsync()
        {
            return View("ViewAllFiles", await _fileService.GetAllAsync());
        }
        [HttpPost]
        public async Task<ActionResult> GetFilesWithFilterAsync(string searchFilter)
        {
            var files = (await _fileService.GetAllAsync())
                .Where(f => f.Name.ToUpper().Contains(searchFilter.ToUpper())
                || f.Description.ToUpper().Contains(searchFilter.ToUpper())
                || f.UserName.ToUpper().Contains(searchFilter.ToUpper()));

            return View("ViewAllFiles", files);
        }
        [HttpPost]
        public async Task<ActionResult> GetUsersWithFilterAsync(string searchFilter)
        {
            ViewBag.AdminId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;

            var users = (await _userService.GetAllAsync())
                .Where(u => u.Username.ToUpper().Contains(searchFilter.ToUpper())
                || u.Email.ToUpper().Contains(searchFilter.ToUpper()));

            return View("ViewAllUsersData", users);
        }
        [HttpGet]
        public ActionResult RegisterNewAdmin()
        {
            return View("RegisterNewAdmin");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterNewAdminAsync(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userByEmail = (await _userService.GetAllAsync()).FirstOrDefault(u => u.Email == model.Email.ToLower());
                var userByName = (await _userService.GetByUsernameAsync(model.Username));

                if (userByName != null)
                {
                    ModelState.AddModelError("Username", "User with the same username already exists!");
                }
                if (userByEmail != null)
                {
                    ModelState.AddModelError("Email", "User with the same email already exists!");
                }
            }
            if (ModelState.IsValid)
            {

                var roleAdminId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;

                UserModel newUser = new UserModel()
                {
                    Username = model.Username,
                    Email = model.Email.ToLower(),
                    Password = model.Password,
                    RoleId = roleAdminId,
                    Gender = GenderModel.Other
                };
                await _userService.AddAsync(newUser);
                return RedirectToAction("Index", "Home");
            }
            return View("RegisterNewAdmin", model);
        }
    }
}