﻿using BLL.DTO;
using BLL.Services;
using BLL.Services.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FileStorage.Controllers
{
    public class ReviewController : Controller
    {
        private readonly IReviewService _reviewService;
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        public ReviewController(IReviewService reviewService, IUserService userService, IRoleService roleService)
        {
            _reviewService = reviewService;
            _userService = userService;
            _roleService = roleService;
        }
        public ReviewController() : this(new ReviewService(), new UserService(), new RoleService())
        {
        }
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            UserModel currentUser;
            try
            {
                currentUser = await _userService.GetByUsernameAsync(this.User.Identity.Name);
            }
            catch(ArgumentException)
            {
                currentUser = null;
            }

            if(currentUser != null)
            {
                ViewBag.CurrentUserId = currentUser.Id;
                var adminRoleId = (await _roleService.GetAllAsync()).FirstOrDefault(r => r.Name == "admin").Id;
                ViewBag.IsAdmin = currentUser.RoleId == adminRoleId;
            }

            return View((await _reviewService.GetAllAsync()).OrderByDescending(r => r.DateTime).ToList());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddReviewAsync(ReviewModel review)
        {
            if (ModelState.IsValid)
            {
                UserModel currentUser;
                try
                {
                    currentUser = await _userService.GetByUsernameAsync(this.User.Identity.Name);
                }
                catch (ArgumentException)
                {
                    currentUser = null;
                }

                string roleName = "";
                if (currentUser != null) 
                {
                    var role = (await _roleService.GetByIdAsync(currentUser.RoleId)).Name;
                    if (role == "admin") roleName = "admin";
                }

                ReviewModel newReview = new ReviewModel()
                {
                    Username = Request.Form["name"] ?? User.Identity.GetUserName(),
                    Text = review.Text,
                    DateTime = DateTime.Now,
                    Role = roleName,
                };

                if (currentUser == null)
                {
                    newReview.UserId = null;
                    newReview.Username = "Guest " + newReview.Username;
                }
                else newReview.UserId = currentUser.Id;

                await _reviewService.AddAsync(newReview);
            }
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> DeleteReviewAsync(Guid reviewId)
        {
            await _reviewService.DeleteAsync(reviewId);
            return RedirectToAction("Index");
        }
    }
}