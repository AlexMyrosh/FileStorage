﻿using BLL.Services;
using BLL.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FileStorage.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IFileDbService _fileService;
        private readonly IFileServerService _fileServerService;
        public UserController(IUserService service, IFileDbService fileService, IFileServerService fileServerService)
        {
            _userService = service;
            _fileService = fileService;
            _fileServerService = fileServerService;
        }
        public UserController() : this(new UserService(), new FileDbService(), new FileServerService())
        {
        }
        [HttpGet]
        [Authorize(Roles = "user")]
        public async Task<ActionResult> ViewUserFilesAsync()
        {
            var files = (await _userService.GetByUsernameAsync(this.User.Identity.Name))
                .Files.OrderByDescending(file => file.DateOfUploading);

            return View("ViewUserFiles", files);
        }
        public async Task<FileResult> DownloadAsync(Guid Id)
        {
            var file = await _fileService.GetByIdAsync(Id);
            byte[] fileBytes = System.IO.File.ReadAllBytes(file.Path);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.Name);
        }
        [Authorize(Roles = "user")]
        public async Task<ActionResult> DeleteAsync(Guid Id)
        {
            var file = await _fileService.GetByIdAsync(Id);
            _fileServerService.DeleteFile(file.Path);
            await _fileService.DeleteAsync(file.Id);
            return RedirectToAction("ViewUserFilesAsync", "User");
        }
        public async Task<ActionResult> FileDetailsAsync(Guid Id)
        {
            var file = await _fileService.GetByIdAsync(Id);
            return View("FileDetails", file);
        }
        [HttpPost]
        public async Task<ActionResult> GetFilesWithFilterAsync(string searchFilter)
        {
            var files = (await _fileService.GetAllAsync())
                .Where(f => f.Name.ToUpper().Contains(searchFilter.ToUpper()));

            return View("ViewUserFiles", files);
        }
    }
}