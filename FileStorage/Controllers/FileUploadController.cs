﻿using BLL.DTO;
using BLL.Services;
using BLL.Services.Interfaces;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FileStorage.Controllers
{
    [Authorize(Roles = "user")]
    public class FileUploadController : Controller
    {
        private readonly IFileDbService _fileService;
        private readonly IUserService _userService;
        private readonly IFileServerService _fileServerService;
        public FileUploadController(IFileDbService fileService, IUserService userService, IFileServerService fileServerService)
        {
            _fileService = fileService;
            _userService = userService;
            _fileServerService = fileServerService;
        }
        public FileUploadController() : this(new FileDbService(), new UserService(), new FileServerService())
        {
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadFileAsync(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid && upload != null)
            {
                var currentUser = await _userService.GetByUsernameAsync(this.User.Identity.Name);
                string privacy = Request.Form["Privacy"] == "false" ? "No" : "Yes";

               

                FileModel newFile = new FileModel()
                {
                    Name = upload.FileName,
                    NameSetByUser = Request.Form["Name"] == String.Empty ? upload.FileName : Request.Form["Name"],
                    Description = Request.Form["Description"],
                    DateOfUploading = DateTime.Now,
                    UserId = currentUser.Id,
                    UserName = currentUser.Username,
                    IsPrivate = privacy
                };

                int theSameFilesCount = 1;
                int i = newFile.Name.LastIndexOf('.');
                Regex thesameFileRegex = new Regex($"^{newFile.Name.Insert(i, "(\\s\\(\\d+\\))?")}$");
                foreach (var item in currentUser.Files)
                    if (thesameFileRegex.IsMatch(item.Name)) theSameFilesCount++;

                if (theSameFilesCount != 1)
                {
                    int index = newFile.Name.LastIndexOf('.');
                    string counter = $" ({theSameFilesCount})";
                    newFile.Name = newFile.Name.Insert(index, counter);
                }
                string root = Path.GetDirectoryName(Path.GetDirectoryName(Server.MapPath("~")));
                string pathToFile = $"{root}\\DAL\\Files\\{currentUser.Username}\\{newFile.Name}";
                newFile.Path = pathToFile;

                await _fileService.AddAsync(newFile);
                _fileServerService.UploadFile(upload, currentUser.Username, newFile.Name);
                return RedirectToAction("ViewUserFilesAsync", "User");
            }
            return View("Index");
        }
    }
}