﻿using System.Web.Mvc;

namespace FileStorage.Controllers
{
    public sealed class ErrorsController : Controller
    {
        public ActionResult NotFound()
        {
            return Request.IsAjaxRequest() ? View("_NotFound") : View();
        }
    }
}