﻿using BLL.Services;
using BLL.Services.Interfaces;
using Ninject.Modules;

namespace FileStorage.DependencyInjection
{
    public class DependencyResolverModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IFileDbService>().To<FileDbService>();
            Bind<IFileServerService>().To<FileServerService>();
            Bind<IReviewService>().To<ReviewService>();
            Bind<IRoleService>().To<RoleService>();
            Bind<IUserService>().To<UserService>();
        }
    }
}